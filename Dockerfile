FROM node:16.13 as build
WORKDIR /app
COPY package.json yarn.lock ./
COPY . ./
RUN yarn
RUN yarn build

FROM nginx:1.21-alpine
COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
