# GUI used in the NOMAD AI Toolkit App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Graphical User Interface (GUI) used for the NOMAD AI Toolkit App. Docker image that is build upon changes in the main branch is pushed to the registry of the NOMAD AI Toolkit App - https://gitlab.mpcdf.mpg.de/nomad-lab/aitoolkit-app. The develop branch builds an image that is employed in the developers' App - https://gitlab.mpcdf.mpg.de/nomad-lab/aitoolkit-develop. The two images can have different tutorials list.
