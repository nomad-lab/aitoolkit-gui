import React, { useMemo } from 'react'
import { useStyles } from './TutorialsPage'
import {
  Button,
  Grid,
  TextField,
  Typography,
  Divider,
  IconButton
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import tutorials from '../toolkitMetadata'
import { StringParam, useQueryParams } from 'use-query-params'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TutorialsIcon from '../images/AIT_ico_bb_tutorial.svg'
import ArrowIcon from '../images/AIT_ico_bd_link_go_to.svg'
import ReproduceIcon from '../images/AIT_ico_bp_replicate.svg'
import AccordionsList from './AccordionsList'
import FigureAI from '../images/AIT_illu_AIT.svg'

export default function ReproducePage() {
  const styles = useStyles()
  const [queryParameters, setQueryParameters] = useQueryParams({
    author: StringParam,
    system: StringParam,
    method: StringParam,
    filterString: StringParam
  })
  const author = queryParameters.author
  const system = queryParameters.system
  const method = queryParameters.method

  const filter = tutorial => {
    const {author, system, method} = queryParameters
    if (author && tutorial.authors.indexOf(author) === -1) {
      return false
    }
    if (system && tutorial.labels.application_system.indexOf(system) === -1) {
      return false
    }
    if (method && tutorial.labels.ai_methods.indexOf(method) === -1) {
      return false
    }
    return true
  }

  const tutorials_list_advanced = tutorials.tutorials.filter(tutorial => tutorial.labels.category[0] === 'advanced_tutorial')

  const {authors, systems, methods} = useMemo(() => {
    const authors = {}
    const systems = {}
    const methods = {}
    tutorials_list_advanced.forEach(tutorial => {
      tutorial.key = tutorial.title.replace(/\W/gm, '_').toLowerCase()
      tutorial.authors.forEach(i => { authors[i] = i })
      tutorial.labels.application_system.forEach(i => { systems[i] = i })
      tutorial.labels.ai_methods.forEach(i => { methods[i] = i })
    }
    )
    return {
      authors: Object.keys(authors).sort(),
      systems: Object.keys(systems).sort(),
      methods: Object.keys(methods).sort()
    }
  }, [tutorials_list_advanced])

  return <Grid container spacing={1} className={styles.root}>
    <Grid container spacing={0} className={styles.Heading}>
      <Grid item xs={6} className={styles.sectionTitle} >
        <Grid container spacing={0}>
          <Grid item xs={4} style={{marginTop: '-100px', marginLeft: '-20px'}}>
            <IconButton
              component={Link}
              to="main">
              <img alt='AI toolkit logo' src={FigureAI} style={{width: '120px'}}/>
            </IconButton>
          </Grid>
          <Grid item xs={8}>
            <Typography className={styles.title}>
              Reproduce published results
            </Typography>
          </Grid>
        </Grid>
        <Typography className={styles.deck}>
          Reproducibility is key in scientific research. The advent of AI-driven big-data analytics has
          introduced a new possible source of uncertainty in scientific communication: The AI workflow are
          typically complex and there is no standard way to describe them. A solution is offered by the
          NOMAD AI toolkit, which hosts open-access notebooks that allow to reproduce result published in
          scientific journals. Explore them below
        </Typography>
      </Grid>
      <Grid item xs={4} className={styles.sectionIcon}>
        <img alt='Reproduce icon' src={ReproduceIcon} className={styles.icon}/>
      </Grid>
    </Grid>
    <Grid container spacing={0}>
      <Grid item xs={12} >
        <Typography className={styles.filter} >
          Filter Tutorials
        </Typography>
      </Grid>
      <Grid item xs={3}>
        <Autocomplete
          id="combo-box-demo"
          options={authors}
          className={styles.autocomplete}
          getOptionLabel={option => option}
          renderInput={params => (
            <TextField
              {...params}
              label="Author"
              InputProps={{...params.InputProps, disableUnderline: true}}
              fullWidth
            />
          )}
          value={author || ""}
          onChange={(_, value) => {
            setQueryParameters({author: value || undefined})
          }}
        />
      </Grid>
      <Grid item xs={3}>
        <Autocomplete
          id="combo-box-demo"
          options={methods}
          className={styles.autocomplete}
          renderInput={params => (
            <TextField
              {...params}
              label="AI Method"
              InputProps={{...params.InputProps, disableUnderline: true}}
              fullWidth
            />
          )}
          value={method || ""}
          onChange={(_, value) => {
            setQueryParameters({method: value || undefined})
          }}
        />
      </Grid>
      <Grid item xs={3}>
        <Autocomplete
          id="combo-box-demo"
          options={systems}
          className={styles.autocomplete}
          getOptionLabel={option => option}
          renderInput={params => (
            <TextField
              {...params}
              label="System"
              InputProps={{...params.InputProps, disableUnderline: true}}
              fullWidth
            />
          )}
          value={system || ""}
          onChange={(_, value) => {
            setQueryParameters({system: value || undefined})
          }}
        />
      </Grid>
    </Grid>
    <Grid container spacing={1} className={styles.tutorialsList}>

      <Grid item xs={12}>
        <Divider
          style={{
            backgroundColor: '#7FEFEF',
            height: '13px',
            borderRadius: '4px',
            marginBottom: '-8px'
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <AccordionsList tutorials_list={tutorials_list_advanced}
          filter={filter}
          setQueryParameters={setQueryParameters}
        />
      </Grid>

    </Grid>
    <Grid item xs={6} className={styles.sectionTitle} >
      <Typography className={styles.titleSecondary}>
        Explore the basics of AI
      </Typography>
      <Typography className={styles.deck}>
        Recent applications of artificial intelligence in science build on top
        of solid methodologies that have been being developed over the last decades.
        Exploring the following tutorials, you can learn the basics of AI to
        better understand their latest applications in materials science.
      </Typography>
      <Grid container spacing={1}>
        <Grid item xs={4}>
          <IconButton
            component={Link}
            to="main"
            style={{marginRight: '0px', marginTop: '20px'}}
          >
            <img alt='AI toolkit logo' src={FigureAI} style={{width: '120px'}}/>
          </IconButton>
        </Grid>
        <Grid item xs={8}>
          <Button
            width='10px'
            color='#2A3C67'
            component={Link}
            to="tutorials"
            className={styles.bottomButton}
            endIcon={<img alt='Arrow icon' src={ArrowIcon}/>}
          >
            <Typography className={styles.bottomButtonText} >
              AI tutorials
            </Typography>
          </Button>
        </Grid>
      </Grid>
    </Grid>
    <Grid item xs={4} className={styles.sectionIcon}>
      <IconButton
        component={Link}
        to="tutorials"
        className={styles.bottomIcon}
      >
        <img alt='Tutorials icon' src={TutorialsIcon} style={{width: '300px'}}/>
      </IconButton>

    </Grid>
  </Grid>
}
