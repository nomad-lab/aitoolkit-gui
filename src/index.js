import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';
import './index.css';
import {Routes} from './routes'

ReactDOM.render(
  <Router>
    <QueryParamProvider ReactRouterRoute={Route}>
      <Routes />
    </QueryParamProvider>
  </Router>,
  document.getElementById('root')
);

