import React, { useEffect } from 'react';
import MainPage  from './components/MainPage';
import ReproducePage from './components/ReproducePage';
import TutorialsPage  from './components/TutorialsPage';
import { 
    BrowserRouter as Router,
    Route, 
    Switch, 
    Redirect,
    withRouter,
    useLocation
} from 'react-router-dom';

function _ScrollToTop(props) {
    const { pathname } = useLocation();    useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);    return props.children
}

const ScrollToTop = withRouter(_ScrollToTop)

export const Routes = () => {

    return (
        <div>
            <Router>
                <ScrollToTop>

                    <Switch>
                        <Route exact path="/Main" component={MainPage}/>
                        < Route exact path="/">
                            <Redirect to="/Main"/>
                            </Route>
                        <Route exact path="/Reproduce" component={ReproducePage}/>
                        <Route exact path="/Tutorials" component={TutorialsPage}/>
                    </Switch>
                </ScrollToTop>

            </Router>
        </div>
    );

};